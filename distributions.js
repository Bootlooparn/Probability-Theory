const math = require('mathjs');

var exports = module.exports = {}

// Uniform distribution
exports.uniform  = function (type,min,max,n) {
  let values = [];

  if (!type) {
    type = 'continuous';
  }
  if(!min) {
    min = 0;
  }
  if (!max) {
    max = 1;
  }
  if (!n) {
    n = 1;
  }

  try {
    let value;
    for (var i = 0; i < n ; i++) {
      if (type === 'continuous') {
        value = Math.random()*(max-min)+min;
      }
      if (type === 'discrete') {
        value = Math.round(Math.random()*(max-min)+min);
      }
      values.push(value);
      if (n === 1) {
        return values;
      }
    }
    return values;
  } catch (e) {
    return 'wrong arguments!';
  }
}

// Normal distribution

exports.normal = function (mu,sigma,n){
  let values = [];

  if (!mu) {
    mu = 0;
  }
  if (!sigma) {
    sigma = 1;
  }
  if (!n) {
    n = 1;
  }

  try {
    let u,v,x;

    for (var i = 0; i < n; i++) {
      u = Math.random();
      v = Math.random();

      z = Math.sqrt(-2*Math.log(u))*Math.sin(2*Math.PI*v)

      x = mu + z*sigma;

      values.push(x);
    }
    return values;
  } catch (e) {
    return 'wrong arguments!';
  }
}

// Exponential distribution
exports.exponential = function (lambda,n) {
  if (!lambda) {
    lambda = 1;
  }
  if (!n) {
    n = 1;
  }
  try {
    let values = exports.uniform('continuous',0,1,n)

    for (var i = 0; i < n; i++) {
      values[i] = - math.multiply(math.divide(1, lambda), math.log(values[i]));
    }
    return values
  } catch (e) {
    return 'wrong arguments!';
  }
}

// Chi squared distribution
exports.chisq = function (df,n) {
  if (!n) {
    n = 1;
  }

  try {
    let values = [];
    let z;

    for (var i = 0; i < n; i++) {
      let value = [];
      for (var j = 0; j < df; j++) {
        z = exports.normal(0,1,1);
        value.push(z.pop());
      }
      value = math.square(value);
      value = math.sum(value);

      values.push(value);
    }
    return values;
  } catch (e) {
    return 'wrong arguments!';
  }
}

// F distribution
exports.fdist = function (df1,df2,n) {
  if (!df1) {
    df1 = 1;
  }
  if (!df2) {
    df2 = 1;
  }
  if (!n) {
    n = 1;
  }

  try {
    let values = [];
    let u,v,f;

    for (var i = 0; i < n; i++) {
      u = exports.chisq(df1,1);
      v = exports.chisq(df2,1);

      f = math.divide(math.divide(u,df1),math.divide(v,df2));
      values.push(f);
    }
    return values;
  } catch (e) {
    return 'wrong arguments!';
  }
}

// Students t-distribution
exports.tdist = function (df,n) {
  if (!df) {
    df = 1;
  }
  if (!n) {
    n = 1;
  }

  try {
    let values = [];
    let z,u,t;

    for (var i = 0; i < n; i++) {
      z = exports.normal(0,1,1);
      u = exports.chisq(df,1);
      t = math.divide(z,math.sqrt(math.divide(u,df)));
      values.push(t);
    }
    return values;
  } catch (e) {
    return 'wrong arguments!';
  }
}

// Gamma distribution
// Weibull distribution
exports.weibull = function (a,b,n) {
  if (!n) {
    n = 1;
  }
  if (!a) {
    a = 1;
  }
  if (!b) {
    b = 1;
  }

  try {
    let values = exports.uniform('continuous',0,1,n);

    for (var i = 0; i < n; i++) {
      values[i] = math.multiply(b,math.power(- math.log(1 - values[i]), math.divide(1, a)));
    }
    return values

  } catch (e) {

  }
}

// Cauchy Distribution
exports.cauchy = function (mu, scale, n) {
  let values = exports.uniform('continuous',0,1,n);

  if (!n) {
    n = 1;
  }
  try {
    for (var i = 0; i < n; i++) {
      values[i] = mu + math.multiply(scale, math.tan(math.multiply(math.pi, values[i] - math.divide(1, 2))));
    }
    return values
  } catch (e) {
    return 'wrong arguments!';
  }
}
// Pareto Distribution

// Logistic Distribution
exports.logistic = function (mu, scale, n) {
  let values = exports.uniform('continuous',0,1,n);

  if (!n) {
    n = 1;
  }
  try {
    for (var i = 0; i < n; i++) {
      values[i] = mu + math.multiply(scale, math.log(math.divide(values[i], 1-values[i])));
    }
    return values
  } catch (e) {
    return 'wrong arguments!';
  }
}
// Double exponential Distribution

return exports
