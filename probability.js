const distributions = require('./distributions');
const processes = require('./processes');
const regressions = require('./regressions');
const test = require('./test');

var exports = module.exports = {}

exports.distributions = distributions
exports.processes = processes
exports.regressions = regressions
