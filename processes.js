const math = require('mathjs');
const distributions = require('./distributions');

// Weiner process
var exports = module.exports = {};

exports.wiener = function (T,N) {
  if (!T) {
    T = 1;
  }
  if (!N) {
    N = 100;
  }

  const dt = T/N;

  let W = [0];

  let dw;

  for (var i = 1; i < N; i++) {
    dw = math.multiply(math.sqrt(dt),distributions.normal(0,1,1));

    W.push(math.sum(W[i-1],dw));
  }

  return W;
}

exports.poisson = function () {

}
